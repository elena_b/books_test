# README

This application was created by using the IDE RubyMine:

* Ruby version 2.7.3 Rails version (~> 6.1.3, >= 6.1.3.2)

* Database in Postgres

To start:

* run ```bundle install```

* run ```bin/rake db:create db:migrate db:seed```

* run ```puma```

