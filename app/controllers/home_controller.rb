class HomeController < ApplicationController
  before_action :get_books_list, :get_filters_info, only: [:index, :update_book_list]

  def index
    if @books.blank?
      return raise ActionController::RoutingError.new('Not Found')
    end
  end

  def create_book
    begin
      @book = Book.new_or_update(create_book_params[:name], create_book_params[:year], create_book_params[:author], create_book_params[:id])
      return render json: { success: true, data: { message: "Книга успешно добавлена"} }
    rescue
      return render json: { success: false, data: { message: "Что-то пошло не так"} }
    end
  end

  def create_book_params
    params.require(:create_book).permit( :name, :year, :author, :id )
  end

  def delete_book
    begin
      Book.destroy(params[:book_id])
      return render json: { success: true, message: "Книга успешно удалена" }
    rescue
      return render json: { success: false, message: "Извините, книга не может быть удалена. Повторите попытку позже." }
    end
  end

  def update_book
    begin
      @current_book = Book.find_by_id(params[:book_id])
      return render json: { success: true, data: {
          book: {
            id: @current_book.id,
            name: @current_book.name,
            year: @current_book.date.strftime("%Y"),
            author: @current_book.author
          },
          message: "Данные для обновления книги возвращены"
        }}
    rescue
      return render json: { success: false, data: { message: "Извините, книга не может быть обновлена. Повторите попытку позже." }}
    end
  end

  def update_book_list
    begin
      year_from = Date.strptime(filter_params[:year_from], '%Y')
      year_to = Date.strptime(filter_params[:year_to], '%Y')
      @books = @books.where('name LIKE ?', "%#{filter_params[:name]}%") unless filter_params[:name].blank?
      @books = @books.where('"books"."date" >= ? AND "books"."date" <= ? ', year_from, year_to)
      @books = @books.where(author: filter_params[:author]) unless filter_params[:author].blank?
      if @books.blank?
        return render json: { success: true, data: { percent:100, html: "<p>Нет книг по данному запросу</p>", message: "Нет книг по данному запросу" }}
      else
        percent = (@books.count*100)/Book.all.count
        html_content = render_to_string(
          partial: 'home/book_list',
          formats: :html,
          layout: false,
          locals: { books: @books}
        )
        return render json: { success: true, data: {
          percent: percent,
          html: html_content,
          message: "Книг по данному запросу #{@books.count}"
        }}
      end
    rescue
      return render json: { success: false, data: { message: "Извините, произошла ошибка. Повторите попытку позже." }}
    end
  end

  def filter_params
    params.require(:filter).permit( :author, :year_from, :year_to, :name )
  end

  def update_filters
    begin
      get_filters_info
      html_content = render_to_string(
        partial: 'home/filter_form',
        formats: :html,
        layout: false,
        locals: { authors: @authors, years: @years }
      )
      return render json: { success: true, data: {
        html: html_content,
        message: "Фильтры успешно обновлены"
      }}
    rescue
      return render json: { success: false, data: { message: "Что-то пошло не так. Фильтры не обновлены."} }
    end
  end

  protected

  def get_books_list
    @books = Book.all.ordered
  end

  def get_filters_info
    @authors = Book.all.pluck(:author).compact.uniq
    @years = Book.all.map{ |book| book.date.strftime("%Y") }.uniq.sort_by{|f| f.to_i}
  end

  # но это я погуглила, не догадалась бы через hash
  def self.get_uniq_element_of_array(arr1 = [1,2,3,5,1,2,3])
    counts = Hash.new(0)
    arr1.each { |v| counts[v] += 1 }
    return counts.select { |v, count| count == 1 }.keys
  end
end