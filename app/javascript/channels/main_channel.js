toggleAddBookForm = function() {
    document.querySelector('.add-book-form').classList.toggle('is--hidden');
}

createBook = function (event) {
    event.preventDefault()
    event.stopPropagation()
    const token = document.getElementsByTagName('meta')['csrf-token'].content;
    const form = document.querySelector('#form-add')
    const data = new FormData(form);
    const idField = form.querySelector('#create_book_id');
    fetch('/create-or-update-book', {
        method: 'POST',
        headers: {
            'X-CSRF-Token': token
        },
        body: data
    })
        .then((response) => { return response.json() })
        .then((result) => {
            if (result.success) {
                console.log(result.data.message);
                form.reset();
                idField.value = '';
                updateBookListSendRequestWithTimer(1000);
                setTimeout(updateFilters(), 1000);
            } else {
                showNotification(result.data.message);
            }
        })
        .catch((error) => {
            showNotification('Извините, произошла ошибка при создании(обновлении книги).');
            console.log(error);
        });
}
updateBook = function (id) {
    const params = {'book_id': encodeURIComponent(id)}
    const token = document.getElementsByTagName('meta')['csrf-token'].content;
    const formWrapper = document.querySelector('.add-book-form');
    const form = formWrapper.querySelector('form');
    const nameField = form.querySelector('#create_book_name');
    const authorField = form.querySelector('#create_book_author');
    const yearField = form.querySelector('#create_book_year');
    const idField = form.querySelector('#create_book_id');
    fetch('/update-book', {
        method: 'POST',
        headers: {
            'Accept': 'application/json, text/plain, */*',
            'Content-Type': 'application/json',
            'X-CSRF-Token': token
        },
        body: JSON.stringify(params)
    })
        .then((response) => { return response.json() })
        .then((result) => {
            if (result.success) {
                console.log(result.data.message);
                nameField.value = result.data.book.name;
                authorField.value = result.data.book.author;
                yearField.value = result.data.book.year;
                idField.value = result.data.book.id;
                nameField.focus();
                if (formWrapper.classList.contains('is--hidden')) {
                    formWrapper.classList.remove('is--hidden');
                }
            } else {
                showNotification(result.data.message);
            }
        })
        .catch((error) => {
            showNotification('Извините, произошла ошибка.');
            console.log(error);
        });
}
deleteBook = function (id) {
    const params = {'book_id': encodeURIComponent(id)}
    const token = document.getElementsByTagName('meta')['csrf-token'].content;
    const row = document.querySelector('.books-list-row[data-book="'+id+'"]');
    const formWrapper = document.querySelector('.add-book-form');
    const form = formWrapper.querySelector('form');
    fetch('/delete-book', {
        method: 'POST',
        headers: {
            'Accept': 'application/json, text/plain, */*',
            'Content-Type': 'application/json',
            'X-CSRF-Token': token
        },
        body: JSON.stringify(params)
    })
    .then((response) => { return response.json() })
    .then((result) => {
        if (result.success) {
            row.remove();
            form.reset();
            formWrapper.classList.add('is--hidden')
            setTimeout(updateFilters(), 1000);
            console.log(result.message);
        } else {
            showNotification(result.message);
        }
    })
    .catch((error) => {
        showNotification('Извините, произошла ошибка при удалении книги.');
        console.log(error);
    });
}
updateBookList = function (event) {
    event.preventDefault()
    event.stopPropagation()
    updateBookListSendRequestWithTimer(0)
}

let BookListSendRequestTimer;
updateBookListSendRequestWithTimer = function (time=0) {
    clearTimeout(BookListSendRequestTimer);
    BookListSendRequestTimer = setTimeout(updateBookListSendRequest, 1000);
}
updateBookListSendRequest = function () {
    const token = document.getElementsByTagName('meta')['csrf-token'].content;
    const form = document.querySelector('#form-filter');
    const data = new FormData(form);
    const resultWrapper = document.querySelector('#js-book-list');
    const diagram = document.querySelector('[stroke-dasharray]');
    fetch('/update-book-list', {
        method: 'POST',
        headers: {
            'X-CSRF-Token': token
        },
        body: data
    })
        .then((response) => { return response.json() })
        .then((result) => {
            if (result.success) {
                console.log(result.data.message);
                resultWrapper.innerHTML = result.data.html;
                diagram.setAttribute("stroke-dasharray", parseInt(result.data.percent)+" "+(100-parseInt(result.data.percent)));
            } else {
                showNotification(result.data.message);
            }
        })
        .catch((error) => {
            showNotification('Извините, произошла ошибка.');
            console.log(error);
        });
}

updateFilters = function () {
    const token = document.getElementsByTagName('meta')['csrf-token'].content;
    const resultWrapper = document.querySelector('#form-filter-wrapper');
    fetch('/update-filters', {
        method: 'POST',
        headers: {
            'X-CSRF-Token': token
        }
    })
        .then((response) => { return response.json() })
        .then((result) => {
            if (result.success) {
                console.log(result.data.message);
                resultWrapper.innerHTML = result.data.html;
            } else {
                showNotification(result.data.message);
            }
        })
        .catch((error) => {
            showNotification('Извините, произошла ошибка 111.');
            console.log(error);
        });
}

showNotification = function (text) {
    if(document.getElementById('notification')) {
        document.querySelector('#notification .notification__message').textContent = text;
        document.getElementById('notification').classList.remove('notification_hidden');
        setTimeout(hideNotification(), 4000);
    }
}
hideNotification = function () {
    if(document.getElementById('notification')) {
        document.getElementById('notification').classList.add('notification_hidden');
    }
}
