class Book < ApplicationRecord
  validates :name, :date, presence: true
  validates :name, uniqueness: true

  scope :ordered, -> { order(created_at: :desc) }

  def self.new_or_update(name, year, author, id)
    self.find_or_create_by(id:id).tap do |book|
      book.name = name
      book.date = Date.strptime(year, '%Y')
      book.author = author
      book.save
    end
  end
end
