require_relative "boot"

require "rails/all"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module BooksTest
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 6.1
    config.site_name = 'BooksTest'
    config.time_zone = 'Krasnoyarsk'
    config.site_link = 'bozina-books.ru'
    # config.i18n.default_locale = :ru
    # config.i18n.available_locales = [:en,:ru]
  end
end
