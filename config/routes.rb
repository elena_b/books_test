Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  post '/create-or-update-book', to: 'home#create_book'
  post '/update-book', to: 'home#update_book'
  post '/delete-book', to: 'home#delete_book'
  post '/update-book-list', to: 'home#update_book_list'
  post '/update-filters', to: 'home#update_filters'
  root "home#index"
end
