class CreateBooks < ActiveRecord::Migration[6.1]
  def change
    create_table :books do |t|
      t.string :name, null:false
      t.date   :date, null:false
      t.string :author
      t.timestamps
    end
  end
end
