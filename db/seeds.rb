# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
#
def time_point_string; Time.now.strftime('%H:%M:%S:%L') end
def year_to_date(input)
  return Date.strptime(input, '%Y')
end
puts "=== #{time_point_string}: Start seeding ==="

puts "#{time_point_string}: Create books"

Book.create!([
                 { name: 'Совершеный код', date: year_to_date('2011'), author: 'Стив Макконелл' },
                 { name: '24 смертных греха компьтерной безопасности', date: year_to_date('2010'), author: 'Д Лебланк' },
                 { name: 'Манипуляция сознанием', date: year_to_date('2006'), author: 'Сергей Кара-Мурза' },
                 { name: 'Приемы объектно-ореентированного программирования', date: year_to_date('2011'), author: 'Д. Влиссидес' },
                 { name: 'Remote. Офис не обязателен', date: year_to_date('2014'), author: 'Джейсон Фрайд' },
                 { name: 'Суад. Сожженная заживо', date: year_to_date('2007') },
                 { name: 'Мост', date: year_to_date('2008'), author: 'Иэн Бенкс' },
                 { name: 'Улица отчаяния', date: year_to_date('2009'), author: 'Иэн Бенкс' },
                 { name: 'Толкование снов', date: year_to_date('2001'), author: 'Зигмунд Фрейд' },
                 { name: 'Самостоятельный ребенок', date: year_to_date('2016'), author: 'Анна Быкова'},
                 { name: 'Код жизни', date: year_to_date('2014'), author: 'Наталья Бехтерева' },
               ])

puts "=== #{time_point_string}: Stop seeding ==="